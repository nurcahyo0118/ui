import { Component } from '@angular/core';
import { NavController, ModalController  } from 'ionic-angular';

import { CategoryPage } from '../category/category';
import { SearchPage } from '../search/search';
import { CartPage } from '../cart/cart';
import { WishlistPage } from '../wishlist/wishlist';
import { ItemdetailPage } from '../itemdetail/itemdetail';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }
  
   slides = [
    {
      title: "Under Rs. 699",
      description: "Tops & Tunics",
      smalltext: "fashion wear of the weeks",
      image: "assets/imgs/slider-1.jpg",
    },
    {
      title: "Under Rs. 699",
      description: "Tops & Tunics",
      smalltext: "fashion wear of the weeks",
      image: "assets/imgs/slider-2.jpg",
    },
    {
      title: "Under Rs. 699",
      description: "Tops & Tunics",
      smalltext: "fashion wear of the weeks",
      image: "assets/imgs/slider-3.jpg",
    }
  ];
  
   categoryPage(){
    this.navCtrl.push(CategoryPage);
    }
    wishlistPage(){
    this.navCtrl.push(WishlistPage);
    }
    
    searchPage() {
    let modal = this.modalCtrl.create(SearchPage);
    modal.present();
  }
  
   cartPage() {
    let modal = this.modalCtrl.create(CartPage);
    modal.present();
  }
  
    itemdetailPage() {
    this.navCtrl.push(ItemdetailPage);
  }

}
