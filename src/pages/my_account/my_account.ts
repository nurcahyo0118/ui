import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { SearchPage } from '../search/search';
import { CartPage } from '../cart/cart';

@Component({
  selector: 'page-my_account ',
  templateUrl: 'my_account.html'
})
export class My_accountPage {
 account: string = "profile";
  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }
  
    searchPage() {
    let modal = this.modalCtrl.create(SearchPage);
    modal.present();
  }
  
  
    cartPage() {
    let modal = this.modalCtrl.create(CartPage);
    modal.present();
  }

}
